# NeverTrustTheUser


The user has to find the password to login to the system. 

**Solution**
Check the site's source code and construct the correct password by reading a javascript code.