<!doctype html>
<html>
  <head>
    <title>Where are the robots</title>
    <link href="https://fonts.googleapis.com/css?family=Monoton|Roboto" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
 	<?php 
	$FLAG = getenv('FLAG');
  	?>
    <div class="container">
      
      <div class="content">
	<p class="lead"></p>
 	<?php 
 		echo '<p style="text-align:center; font-size:30px;"><b>'.$FLAG.'</b></p>';
	?>
      </div>
      <footer></footer>
  </body>
</html>

